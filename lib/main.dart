import 'package:flutter/material.dart';

import 'db_helper.dart';
import 'user.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'SESI 12'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final nameController = TextEditingController();
  final dbHelper = DatabaseHelper.instance;
  List<User> users = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
              height: 70,
              child: ListTile(
                title: TextField(
                  controller: nameController,
                  decoration: const InputDecoration(
                    labelText: 'Name',
                  ),
                ),
                trailing: IconButton(
                    onPressed: () {
                      String name = nameController.text;
                      insert(name);
                    },
                    icon: const Icon(
                      Icons.add_circle_sharp,
                      size: 30,
                      color: Colors.blue,
                    )),
              )),
          const Divider(),
          Expanded(
            flex: 1,
            child: RefreshIndicator(
              onRefresh: () async {
                await Future.delayed(const Duration(seconds: 1));
                setState(() {
                  queryAll();
                });
              },
              child: ListView.builder(
                padding: const EdgeInsets.all(8),
                itemCount: users.length,
                itemBuilder: (BuildContext context, int index) {
                  return SizedBox(
                    child: Card(
                      child: Padding(
                        padding: const EdgeInsets.all(18.0),
                        child: Text(users[index].name ?? ''),
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  void queryAll() async {
    final allRows = await dbHelper.queryAll();
    users.clear();
    for (var row in allRows) {
      users.add(User.fromMap(row));
    }
    setState(() {});
  }

  void insert(String name) async {
    Map<String, dynamic> row = {
      DatabaseHelper.columnName: name,
    };
    User user = User.fromMap(row);
    final id = await dbHelper.insert(user);
    _showMessageInScaffold('User $id has been saved');
    queryAll();
  }

  void _showMessageInScaffold(String message) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(message),
    ));
  }
}
