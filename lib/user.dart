import 'db_helper.dart';

class User {
  int? id;
  String? name;

  User(this.id, this.name);

  User.fromMap(Map<String, dynamic> map) {
    id = map['id'];
    name = map['name'];
  }

  Map<String, dynamic> toMap() {
    return {
      DatabaseHelper.columnId: id,
      DatabaseHelper.columnName: name,
    };
  }
}
